# springboot+layui+mybatisplus 通用权限管理系统

#### 介绍
首先说明的是本人最早学的java，2010年学的ssh框架，后来因为太麻烦所以自学了php，换工作后一直从事php，java也放弃了，但是今年4月份到了新公司，公司内部java项目组缺人，叫我转到java，就这样了干到现在有快四个月了，在这四个月时间里，自己就是个小菜鸟，所以我一直在想自己开发一个基础的java权限框架出来供大家免费使用，现在第一步已经完成了，希望大家多多支持！
#### 软件架构
软件架构说明
系统采用springboot+layui+mybatisplus技术开发，权限部分用的java的shiro框架进行验证，
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170140_ff2a1ab1_383370.png "TIM截图20190729170117.png")
如上图所示，权限部分的代码都是自己一行一行撸的，由于以前用php的时候权限就是自己一点点写出来，所以java中原理类似吧
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170413_d1ccd142_383370.png "TIM截图20190729164610.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170434_395c4dba_383370.png "TIM截图20190729164638.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170442_4d7e1ade_383370.png "TIM截图20190729164658.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170448_8c0ba669_383370.png "TIM截图20190729164712.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170456_7bc286bf_383370.png "TIM截图20190729164726.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170505_f355d9df_383370.png "TIM截图20190729164741.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170526_7c40f693_383370.png "TIM截图20190729164752.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170543_32b18b82_383370.png "TIM截图20190729164801.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170556_a0fd1703_383370.png "TIM截图20190729164817.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170612_859bf60f_383370.png "TIM截图20190729164823.png")

#### 安装教程

1. 大家下载码云的代码放到本地
2. 里面的sql直接导入到数据库中
3. 用idea导入并启动项目
如果大家喜欢，给点经济支持谢谢，我会继续完善本基础框架的，下一步打算给用户加上部门管理
![输入图片说明](https://images.gitee.com/uploads/images/2019/0729/170838_bc522b6f_383370.jpeg "TIM图片20190729170740.jpg")

用了近两周时间做了一个java的基础权限管理框架主要技术是springboot+layui+mybatiusplus+shiro，开始从php转过来，以前php权限都是自己手写权限，现在转到java所以也就想从底层慢慢写出来，现在终于完成了，我把它开源出来，欢迎大家下载使用，有什么问题可以再码云项目下面给我留言哦，下面是项目的开源地址