package com.example.controller;
/***
 * 外网跳转内网对应万能接口
 * 外网访问index1  然后跳转到内网的index2  参数可以全部接收到
 */

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import com.example.mapper.SysMenuMapper;
import com.example.mapper.SysUserMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/server")
public class ServerRedirectController {

    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private SysMenuMapper sysMenuMapper;

    @RequestMapping("/index")
    @ResponseBody
    public JSONObject index(HttpServletRequest request, Map<String, Object> map) {
        JSONObject jsonObject = new JSONObject();
        HashMap<String, Object> paramMap = new HashMap<>();
        Map<String, String[]> parameterMap = request.getParameterMap();
        for(String key : parameterMap.keySet()){
            String[] value = parameterMap.get(key);
            paramMap.put(key, value[0]);
        }
        String result = HttpUtil.post("http://localhost:8081/springbootdemo/weixin/index2", paramMap);
        jsonObject.put("code", 200);
        jsonObject.put("data", result);
        jsonObject.put("msg", "成功");
        return jsonObject;
    }

    @RequestMapping("/index2")
    @ResponseBody
    public JSONObject index2(HttpServletRequest request, Map<String, Object> map) {
        JSONObject jsonObject = new JSONObject();
        Map<String, String[]> parameterMap = request.getParameterMap();

        jsonObject.put("code", 200);
        jsonObject.put("data", parameterMap);
        jsonObject.put("msg", "成功");
        return jsonObject;
    }
}
