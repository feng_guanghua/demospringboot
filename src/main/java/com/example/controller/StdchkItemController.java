package com.example.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.entity.SysUser;
import com.example.mapper.StdchkItemMapper;
import com.example.utils.ShiroUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("/stdchkitem")
public class StdchkItemController {

    @Resource
    private StdchkItemMapper stdchkItemMapper;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {

        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        String method = request.getMethod();
        if ("POST".equals(method)) {
            ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());

            Integer pageNo = Integer.valueOf(request.getParameter("page"));
            Integer pageSize = Integer.valueOf(request.getParameter("pageSize"));
            Page<Map> page = new Page<Map>(pageNo,pageSize);
            QueryWrapper<Map<String,Object>> wrapper = new QueryWrapper<>();


            IPage<Map<String,Object>> stdchkItems= stdchkItemMapper.selectListAndCatgoryPage(page);


            //List<Map<String,Object>> stdchkItems = stdchkItemMapper.selectListAndCatgory();
            modelAndView.addObject("data",stdchkItems);
            modelAndView.addObject("sysuser", sysUser);
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("page/stdchkitem/stdchkitem");
            modelAndView.addObject("sysuser", sysUser);
            return modelAndView;
        }
    }
}
