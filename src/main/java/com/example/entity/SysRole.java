package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.List;

@Data
public class SysRole {
    @TableId(value = "id",type = IdType.AUTO)
    private long id;
    private String name;
    private String remark;
    private long createUserId;
    private java.sql.Timestamp createTime;

    @TableField(exist = false)
    private List<SysMenu> listMenu;

}
