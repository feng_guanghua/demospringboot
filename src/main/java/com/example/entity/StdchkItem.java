package com.example.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class StdchkItem {

  private long id;
  private long number;
  private long categoryId;
  @TableField("categoryName")
  private String categoryName;
  private String serialno;
  private String demand;
  private String standard;
  private String methodContent;
  private String requiredInfo;
  private String detailedRules;
  private double score;
  private String remark;
  private long enabled;
  @TableField("defResponsible_id")
  private long defResponsibleId;
  private long deleted;
  private long flags;

}
