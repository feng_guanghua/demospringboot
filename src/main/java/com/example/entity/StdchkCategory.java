package com.example.entity;

import lombok.Data;

@Data
public class StdchkCategory {

  private long id;
  private long number;
  private long parentId;
  private String serialno;
  private String name;
  private String remark;
  private long deleted;
  private double weight;

}
