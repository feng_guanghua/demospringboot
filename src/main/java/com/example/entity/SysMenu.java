package com.example.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.List;

@Data
public class SysMenu extends Model<SysMenu> {

    @TableId
    private long id;
    private long parentId;
    private String name;
    private String url;
    private String perms;
    private long type;
    private String icon;
    private long orderNum;

    @TableField(exist = false)
    private String checkArr;
    @TableField(exist = false)
    private List<SysMenu> children;

}
