package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.List;

@Data
public class SysUser {

  @TableId(value = "id",type = IdType.AUTO)
  private long id;
  private String username;
  private String password;
  private String salt;
  private String email;
  private String mobile;
  private long sex;
  private String pic;
  private long status;
  private long createUserId;
  private java.sql.Timestamp createTime;
  private String remark;

  @TableField(exist = false)
  private List<SysRole> listRole;



}
