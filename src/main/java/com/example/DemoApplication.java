package com.example;

import cn.hutool.cron.CronUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@SpringBootApplication()
@MapperScan("com.example.mapper")
public class DemoApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        CronUtil.start();
        CronUtil.setMatchSecond(true);
        //System.out.println("Java Virtual Machine Specification信息---------------------------");
        //System.out.println(SystemUtil.getJvmSpecInfo());
        //System.out.println("Java Virtual Machine Implementation信息---------------------------");
        //System.out.println(SystemUtil.getJvmInfo());
        //System.out.println("Java Specification信息---------------------------");
        //SystemUtil.getJavaSpecInfo();
        //System.out.println("Java Implementation信息:");
        //System.out.println(SystemUtil.getJavaInfo());
        //System.out.println("Java运行时信息");
        //System.out.println(SystemUtil.getJavaRuntimeInfo());
        //System.out.println("系统信息");
        //System.out.println(SystemUtil.getOsInfo());
        //System.out.println("用户信息");
        //System.out.println(SystemUtil.getUserInfo());
        //System.out.println("当前主机网络地址信息");
        //System.out.println(SystemUtil.getHostInfo());
        //System.out.println("运行时信息，包括内存总大小、已用大小、可用大小等");
        //System.out.println(SystemUtil.getRuntimeInfo());
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return super.configure(builder);
    }
}
