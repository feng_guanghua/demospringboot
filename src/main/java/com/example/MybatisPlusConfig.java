package com.example;

import com.baomidou.mybatisplus.autoconfigure.MybatisPlusProperties;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import com.mybatis.jpa.xml.XMLMapperLoader;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@Configuration
@MapperScan("com.example.mapper.*")//输入你的dao层的包
public class MybatisPlusConfig {

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * SQL执行效率插件
     */
    @Bean
    //  @Profile({ "dev", "test" }) // 设置 dev test 环境开启
    public PerformanceInterceptor performanceInterceptor() {
        return new PerformanceInterceptor();
    }

    /**
     * 自动热价在mapper.xml
     * @param plusProperties
     * @return
     */
    @Bean
    public XMLMapperLoader getXMLMapperLoader(MybatisPlusProperties plusProperties){
        XMLMapperLoader loader = new XMLMapperLoader();
        loader.setEnabled(true);//开启xml热加载
        loader.setMapperLocations(plusProperties.resolveMapperLocations());
        //LOG.info("xml刷新器初始化:" + plusProperties.getMapperLocations() + "--" + loader.getMapperLocations());
        return loader;
    }
}