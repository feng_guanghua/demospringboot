function Map() {
    this.obj = {};
    this.count = 0;
}

Map.prototype.put = function (key, value) {
    var oldValue = this.obj[key];
    if (oldValue == undefined) {
        this.count++;
    }
    this.obj[key] = value;
}
Map.prototype.get = function (key) {
    return this.obj[key];
}
Map.prototype.remove = function (key) {
    var oldValue = this.obj[key];
    if (oldValue != undefined) {
        this.count--;
        delete this.obj[key];
    }
}
Map.prototype.size = function () {
    return this.count;
}

//获得本年的开始日期
function getYearStartDate() {
    //获得当前年份4位年
    var currentYear = new Date().getFullYear();
    //本年第一天
    var currentYearFirstDate = new Date(currentYear, 0, 1);
    return formatDate(currentYearFirstDate);
}

//获得本年的结束日期
function getYearEndDate() {
    //获得当前年份4位年
    var currentYear = new Date().getFullYear();
    //本年最后
    var currentYearLastDate = new Date(currentYear, 11, 31);
    return formatDate(currentYearLastDate);
}

//获得本月的开始日期
function getMonthStartDate() {
    var Nowdate = new Date();
    var MonthFirstDay = new Date(Nowdate.getFullYear(), Nowdate.getMonth(), 1);
    M = Number(MonthFirstDay.getMonth()) + 1
    return MonthFirstDay.getFullYear() + "-" + M + "-" + MonthFirstDay.getDate();
}

//获得本月的结束日期
function getMonthEndDate() {
    var Nowdate = new Date();
    var MonthNextFirstDay = new Date(Nowdate.getFullYear(), Nowdate.getMonth() + 1, 1);
    var MonthLastDay = new Date(MonthNextFirstDay - 86400000);
    M = Number(MonthLastDay.getMonth()) + 1
    return MonthLastDay.getFullYear() + "-" + M + "-" + MonthLastDay.getDate();
}

//本周开始时间
function getWeekStartDate() {
    var Nowdate = new Date();
    var WeekFirstDay = new Date(Nowdate - (Nowdate.getDay() - 1) * 86400000);
    M = Number(WeekFirstDay.getMonth()) + 1
    return WeekFirstDay.getFullYear() + "-" + M + "-" + WeekFirstDay.getDate();
}

//本周结束时间
function getWeekEndDate() {
    var Nowdate = new Date();
    var WeekFirstDay = new Date(Nowdate - (Nowdate.getDay() - 1) * 86400000);
    var WeekLastDay = new Date((WeekFirstDay / 1000 + 6 * 86400) * 1000);
    M = Number(WeekLastDay.getMonth()) + 1
    return WeekLastDay.getFullYear() + "-" + M + "-" + WeekLastDay.getDate();
}

//获得上周起始
function getPreWeekStartDate() {
    var getUpWeekStartDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - new Date().getDay() - 6);
    var startTime = formatDate(getUpWeekStartDate);
    return startTime;
}

//获得上周结束
function getPreWeekEndDate() {
    var getUpWeekEndDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + (6 - new Date().getDay() - 6));
    var endTime = formatDate(getUpWeekEndDate);
    return endTime;
}

//获取本旬的开始时间
function getXunStartDate() {
    var date = new Date().getDate();//获取当前几号
    if (date < 11) {
        var tenbeginDate = getMonthStartDate();
        return tenbeginDate;
    } else if (date < 21) {
        var tenbeginDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + 11;
        return tenbeginDate;
    } else {
        var tenbeginDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + 21;
        return tenbeginDate;
    }
}

//获取本旬的结束时间
function getXunEndDate() {
    var date = new Date().getDate();//获取当前几号
    if (date < 11) {
        var tenEndDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + 10;
        return tenEndDate;
    } else if (date < 21) {
        var tenEndDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + 20;
        return tenEndDate;
    } else {
        var tenEndDate = getMonthEndDate();
        return tenEndDate;
    }
}

//格式化日期：yyyy-MM-dd
function formatDate(date) {
    var date = new Date(date);
    var myyear = date.getFullYear();
    var mymonth = date.getMonth() + 1;
    var myweekday = date.getDate();
    if (mymonth < 10) {
        mymonth = "0" + mymonth;
    }
    if (myweekday < 10) {
        myweekday = "0" + myweekday;
    }
    return (myyear + "-" + mymonth + "-" + myweekday);
}

// 时间戳转换时间
function formatDateTime(timeStamp) {
    var date = new Date(timeStamp);
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return y + '-' + m + '-' + d + ' ' + h + ":" + minute + ":" + second;
};

//获得某月的天数
function getMonthDays(myMonth) {
    var monthStartDate = new Date(nowYear, myMonth, 1);
    var monthEndDate = new Date(nowYear, myMonth + 1, 1);
    var days = (monthEndDate - monthStartDate) / (1000 * 60 * 60 * 24);
    return days;
}

function getSortArray(arr) {
    arr=arr.sort(function (a, b) {
        return a - b;
    }); // [5,12,22,25,51,56]
    return arr;
    //var min = arr[0];  // 5
    //var max = arr[arr.length - 1];  // 56
}